const fs = require('fs');
const util = require('util');
const config = require('./config.js');
const path = require('path');
const watch = require('node-watch');
const Koa = require('koa');
const Router = require('koa-router');
const serve = require('koa-static');
const IO = require('koa-socket-2');

const app = new Koa();
const io = new IO();

io.attach(app);

const stat = util.promisify(fs.stat);
const readdir = util.promisify(fs.readdir);

(async () => {
    let images = await getImages();
    console.log(images);
})();

//start watch for file changes
// let watcher = watch(config.dir, { filter: /.(jpg|gif|jpeg|png)/g }, handleFileChange);
let watcher = watch(config.dir, handleFileChange);
async function handleFileChange(evt, filename) {
    if (evt === 'update') {
        try {
            let parsed = path.parse(filename);
            if (/.(jpg|gif|jpeg|png)/.test(parsed.ext)) {
                app.io.broadcast('file:update', {
                    absolute: filename,
                    details: parsed
                });
            }
        } catch(err) {
            console.log(err);
        }
    }

    if (evt === 'remove') {
        try {
            let parsed = path.parse(filename);
            if (/.(jpg|gif|jpeg|png)/.test(parsed.ext)) {
                app.io.broadcast('file:removed', {
                    absolute: filename,
                    details: parsed
                });
            }
        } catch(err) {
            console.log(err);
        }
    }
}

/**
 * Gets all the current images in the watched directory to send over as an intial connection payload to a client
 * @return {array} Array of object representing files in the directory
 */
async function getImages() {
    try {
        let files = await readdir(config.dir);
        return files.filter(filename => {
            return /.(jpg|gif|jpeg|png)/.test(filename);
        });
    } catch(err) {
        console.log(err);
    }
}

app.use(serve(`${__dirname}/public`));

app.listen(5000, _ => { console.log(`server running on port 5000`);})
